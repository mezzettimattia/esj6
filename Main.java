package com.company;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int giorno1,giorno2,mese1,mese2,anno1,anno2;
        Scanner scanner=new Scanner(System.in);

        System.out.println("Inserisci il giorno della prima data");
        giorno1=scanner.nextInt();
        System.out.println("Inserisci il mese della prima data");
        mese1=scanner.nextInt();
        System.out.println("Inserisci l'anno della prima data");
        anno1=scanner.nextInt();

        System.out.println("Inserisci il giorno della seconda data");
        giorno2=scanner.nextInt();
        System.out.println("Inserisci il mese della seconda data");
        mese2=scanner.nextInt();
        System.out.println("Inserisci l'anno della seconda data");
        anno2=scanner.nextInt();

        Instant start = Instant.now();
        LocalDate data1=LocalDate.now(),data2=LocalDate.now();

        try{
            data1=LocalDate.of(anno1,mese1,giorno1);
            data2=LocalDate.of(anno2,mese2,giorno2);
        } catch (DateTimeException exception){
            System.out.println("Una delle due date potrebbe non essere valida");
            System.exit(1);
        }

        System.out.println(data1.format(DateTimeFormatter.ofPattern("YYYY MMMM dd", Locale.ITALIAN)));
        System.out.println(data1.format(DateTimeFormatter.ofPattern("YYYY MMMM dd", Locale.FRENCH)));
        System.out.println(data1.format(DateTimeFormatter.ofPattern("YYYY MMMM dd", Locale.JAPANESE)));

        System.out.println(data2.format(DateTimeFormatter.ofPattern("YYYY MMMM dd", Locale.ITALIAN)));
        System.out.println(data2.format(DateTimeFormatter.ofPattern("YYYY MMMM dd", Locale.FRENCH)));
        System.out.println(data2.format(DateTimeFormatter.ofPattern("YYYY MMMM dd", Locale.JAPANESE)));

        System.out.println(Period.between(LocalDate.now(),data1).getDays());
        System.out.println(Period.between(LocalDate.now(),data2).getDays());
        System.out.println(Period.between(data1,data2).getDays());

        Instant finish = Instant.now();

        System.out.println(Duration.between(start,finish).toMillis());

    }
}
